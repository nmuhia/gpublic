from django.core.context_processors import csrf
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response, redirect
from exams.models import User, Response, QUnit, Question, Exam, Log, Record, UserForm
from clients.models import Program
from clients.helpers import loggedIn
from exams import textflow

from exams.textflow import helpers

from django.utils import timezone
from datetime import datetime, timedelta
from django.core import serializers
from django.template import RequestContext

def receiveSMS(req):
  try:
    # extract the info
    destination = req.POST['destination']
    source = req.POST['source']
    message = req.POST['message']
    # log it first of all
    l = Log(destination=destination, source=source, message=message)
    l.save()
    # set up textflow and let it do the rest of the work
    
    machine = textflow.create()
    machine.handleSMS(destination, source, message)
    # done, everything else will be handled by textflow
    return HttpResponse()
  except KeyError:
    # bad request, do nothing
    return HttpResponse()

def smsLog(req):
  # send the logs as json
  jsonData = serializers.serialize("json", Log.objects.all(), fields=('source', 'destination', 'message'))
  return HttpResponse(jsonData, content_type="application/json")

# Le cool authenticators
def examAuth(fn):
  def wrapped(client, *args, **kargs):
    exam = Exam.objects.get(pk=int(kargs['id']))
    auth = exam.program.client
    if client == auth:
      return fn(client, *args, **kargs)
    else:
      return redirect('/')
  return wrapped

def qunitAuth(fn):
  def wrapped(client, *args, **kargs):
    qunit = QUnit.objects.get(pk=int(kargs['id']))
    auth = qunit.exam.program.client
    if client == auth:
      return fn(client, *args, **kargs)
    else:
      return redirect('/')
  return wrapped

# authorized to access question with pk=id
def qAuth(fn):
  def wrapped(client, *args, **kargs):
    q = Question.objects.get(pk=int(kargs['id']))
    auth = q.qunit.exam.program.client
    if client == auth:
      # ok all good
      return fn(client, *args, **kargs)
    else:
      return redirect('/')
  return wrapped

def userAuth(fn):
  def wrapped(client, *args, **kargs):
    user = User.objects.get(pk=int(kargs['id']))
    auth = user.program.client
    if client == auth:
      return fn(client, *args, **kargs)
    else:
      return redirect('/')
  return wrapped

@loggedIn
def examForm(client, req, id, mode="new"):
  ctx = {}
  ctx.update(csrf(req))
  ctx['client'] = client
  if mode == "new":
    program = Program.objects.get(pk=int(id))
    if program.client != client:
      # not authorized!
      return redirect('/')
    ctx['program'] = program
  
  if mode == "edit":
    exam = Exam.objects.get(pk=int(id))
    if exam.program.client != client:
      return redirect('/')
    ctx['exam'] = exam
  
  return render_to_response('exams/examForm.html', ctx, RequestContext(req, {}))

def notifyExam(client, exam):
  # make sure this exam isn't out of time
  now = timezone.make_aware(datetime.now(), timezone.get_default_timezone())
  if exam.endTime < now:
    return # do nothing
  # get the program / users with state = 'done'
  users = exam.program.users.filter(state='done')
  notDones = [user for user in users if not helpers.completed(user, exam)]
  print notDones
  # set their states to 'startExam' and tell them about it
  for user in notDones:
    user.state = 'startExam'
    user.save()
    textflow.send_sms(client, user, "There's a new exam! Send 'ok' to begin.")

@loggedIn
def createExam(client, req):
  title = req.POST['title']
  description = req.POST['description']
  programId = req.POST['programId']
  program = Program.objects.get(pk=int(programId))
  startStr = req.POST['startDate']+" "+"00:00"
  endStr = req.POST['endDate']+" "+"00:00"
  startTime = datetime.strptime(startStr, "%Y-%m-%d %H:%M")
  endTime = datetime.strptime(endStr, "%Y-%m-%d %H:%M")
 # startStr = req.POST['startDate']
  #endStr = req.POST['endDate']
  print startTime
  print endTime 
  #startTime = datetime.strptime(startStr, '%Y-%m-%d')
  #endTime = datetime.strptime(endStr, "%y-%m-%d")
  # make the times timezone aware
  startTime = timezone.make_aware(startTime, timezone.get_default_timezone())
  endTime = timezone.make_aware(endTime, timezone.get_default_timezone())
  '''
  if 'ready' in req.POST and req.POST['ready'] == 'yes':
    ready = True
    # notify all the students who are done
    notifyExam(client, exam)
  else:
    ready = False
  '''
  ready = True
    
  exam = Exam(title=title, description=description, program=program,
             startTime=startTime, endTime=endTime, ready=ready)
 
  #exam = Exam(title=title, description=description, program=program,
              #startTime=startTime, endTime=endTime, ready=ready)
	
  exam.save()
  return redirect('/exams/'+str(exam.id), RequestContext(req, {}))

def genExamReport(exam):
  users = exam.program.users.all()
  qunits = exam.qunits.all()
  reps = []
  for user in users:
    rep = {"user": user, "scores": []}
    totalRight = 0
    totalQs = 0
    for qunit in qunits:
      # get the score
      score = {}
      right = Response.objects.filter(user=user, qunit=qunit, correct=True).count()
      total = qunit.questions.count()
      totalRight += right
      totalQs += total
      score["right"] = right
      score["total"] = total
      rep["scores"].append(score)
    rep["totalRight"] = totalRight
    rep["totalQs"] = totalQs
    reps.append(rep)
  
  return reps

 
@loggedIn
#@examAuth
def viewreport(client, req):  
  examz = Exam.objects.all()
  exam_id = req.GET['exam']
  print exam_id
  exam = Exam.objects.get(pk=int(exam_id))
  print examz
  program = exam.program
  ctx = {}
  ctx['reports'] = genExamReport(exam)
  ctx['qunits'] = exam.qunits.all()
  ctx['examz'] = examz
  ctx['exam'] = exam
  ctx['program'] = program
  ctx['client'] = client
  return render_to_response('pages/reports.html', ctx, RequestContext(req, {}))

@loggedIn
@examAuth
def reports(client, req, id):
  exam = Exam.objects.get(pk=int(id))
  examz = Exam.objects.all()
  program = exam.program
  ctx = {}
  ctx['reports'] = genExamReport(exam)
  ctx['qunits'] = exam.qunits.all()
  ctx['exam'] = exam
  ctx['examz'] = examz
  ctx['program'] = program
  ctx['client'] = client
  return render_to_response('pages/reports.html', ctx, RequestContext(req, {}))
  
  
@loggedIn
@examAuth
def viewExam(client, req, id):
  exam = Exam.objects.get(pk=int(id))
  examz = Exam.objects.all()
  program = exam.program
  ctx = {}
  ctx['reports'] = genExamReport(exam)
  ctx['qunits'] = exam.qunits.all()
  ctx['exam'] = exam
  ctx['examz'] = examz
  ctx['program'] = program
  ctx['client'] = client
  return render_to_response('exams/viewExam.html', ctx, RequestContext(req, {}))
  
 
@loggedIn
@examAuth
def updateExam(client, req, id):
  exam = Exam.objects.get(pk=int(id))
  exam.title = req.POST['title']
  exam.description = req.POST['description']
  startStr = req.POST['startDate']+" "+req.POST['startHour']+":"+req.POST['startMinute']
  endStr = req.POST['endDate']+" "+req.POST['endHour']+":"+req.POST['endMinute']
  startTime = datetime.strptime(startStr, "%d/%m/%y %H:%M")
  endTime = datetime.strptime(endStr, "%d/%m/%y %H:%M")
  # make the times timezone aware
  exam.startTime = timezone.make_aware(startTime, timezone.get_default_timezone())
  exam.endTime = timezone.make_aware(endTime, timezone.get_default_timezone())
  print "startTime: "+str(startTime)
  if 'ready' in req.POST and req.POST['ready'] == 'yes':
    exam.ready = True
    # notify all the students who are done
    notifyExam(client, exam)
  else:
    exam.ready = False
  exam.save()
  return redirect('/exams/'+str(exam.id), RequestContext(req, {}))

@loggedIn
@examAuth
def deleteExam(client, req, id):
  exam = Exam.objects.get(pk=int(id))
  program = exam.program
  exam.delete()
  return redirect('/programs/'+str(program.id))

@loggedIn
def qUnitForm(client, req, id, mode="new"):
  ctx = {}
  ctx.update(csrf(req))
  ctx['client'] = client
  ctx['myid'] = id
  
  if mode == "new":
    exam = Exam.objects.get(pk=int(id))
    # make sure we're allowed to edit this exam
    auth = exam.program.client
    if client != auth:
      return redirect('/home')
    ctx['exam'] = exam
    ctx['program'] = exam.program
  
  if mode == "edit":
    qunit = QUnit.objects.get(pk=int(id))
    # make sure we're allowed to edit this qunit
    auth = qunit.exam.program.client
    if client != auth:
      return redirect('/home')
    ctx['qunit'] = qunit
    ctx['exam'] = qunit.exam
    ctx['program'] = qunit.exam.program
    
  return render_to_response('exams/qUnitForm.html', ctx, RequestContext(req, {}))

@loggedIn
def createQUnit(client, req):
  title = req.POST['title']
  description = req.POST['description']
  examId = req.POST['examId']
  
  exam = Exam.objects.get(pk=int(examId))
  # figure out what order the quiz unit should be
  order = exam.qunits.count()
  # now make the qunit!
  qunit = QUnit(title=title, description=description, exam=exam, order=order)
  qunit.save()
  print qunit
  # redirect to the new quiz unit
  return redirect('/exams/qunits/'+str(qunit.id), RequestContext(req, {}))

@loggedIn
@qunitAuth
def updateQUnit(client, req, id):
  ctx = {}
  ctx.update(csrf(req))
  ctx['client'] = client
  qunit = QUnit.objects.get(pk=int(id))
  qunit.title = req.POST['title']
  qunit.description = req.POST['description']
  qunit.order = req.POST['order']
  qunit.save()
  return redirect("/exams/qunits/"+str(qunit.id),  RequestContext(req, {}))

@loggedIn
@qunitAuth
def deleteQUnit(client, req, id):
  qunit = QUnit.objects.get(pk=int(id))
  # get the exam so we know where to redirect to
  exam = qunit.exam
  qunit.delete()
  return redirect("/exams/"+str(exam.id))

@loggedIn
@qunitAuth
def viewQUnit(client, req, id):
  ctx = {}
  ctx.update(csrf(req))
  ctx['client'] = client
  qunit = QUnit.objects.get(pk=int(id))
  exam = qunit.exam
  program = exam.program
  ctx = {}
  ctx.update(csrf(req))
  ctx['qunit'] = qunit
  ctx['exam'] = exam
  ctx['program'] = program
  ctx['client'] = client
  return render_to_response('exams/viewQUnit.html', ctx,RequestContext(req, {}))


@loggedIn
def createQ(client, req):
  ctx = {}
  ctx.update(csrf(req))
  ctx['client'] = client
  
  lastentry=req.POST['lastentry']
  numentry=int(lastentry)

  while(numentry>0):	
	text=req.POST['qt'+str(numentry)]
	content=req.POST['ct'+str(numentry)]
	choices=req.POST['choices'+str(numentry)]
	correct=req.POST['correct'+str(numentry)]		
  	qunitId = req.POST['qunitId'] 
  	qunit = QUnit.objects.get(pk=int(qunitId))  	
  	q = Question(text=text, content=content, choices=choices, correct=correct, qunit=qunit)
  	q.save()
	numentry=numentry-1;
  
  return redirect("/exams/qunits/"+qunitId, RequestContext(req, {}))

@loggedIn
@qAuth
def viewQ(client, req, id):
  q = Question.objects.get(pk=int(id))
  
  # need to get the rightCount and wrongCount
  rightResp = Response.objects.filter(question=q, correct=True).all()
  wrongResp = Response.objects.filter(question=q, correct=False).all()
  rights = [r.user for r in rightResp]
  wrongs = [w.user for w in wrongResp]
  ctx = {}
  
  ctx['rights'] = rights
  ctx['wrongs'] = wrongs
  
  ctx['question'] = q
  ctx['qunit'] = q.qunit
  ctx['exam'] = q.qunit.exam
  ctx['program'] = q.qunit.exam.program
  ctx['client'] = client
  return render_to_response('exams/viewQ.html', ctx, RequestContext(req, {}))

'''
@loggedIn
@qunitAuth
'''
def performance(req):
  ctx = {}
  ctx.update(csrf(req))
  return render_to_response('exams/performance.html', ctx,RequestContext(req, {}))


@loggedIn
@qAuth
def qForm(client, req, id):
  ctx = {}
  ctx.update(csrf(req))
  
  q = Question.objects.get(pk=int(id))
  ctx['question'] = q
  ctx['qunit'] = q.qunit
  ctx['exam'] = q.qunit.exam
  ctx['program'] = q.qunit.exam.program
  ctx['client'] = client
  return render_to_response('exams/qForm.html', ctx, RequestContext(req, {}))

@loggedIn
@qAuth
def updateQ(client, req, id):
  q = Question.objects.get(pk=int(id))
  q.text = req.POST['text']
  q.choices = req.POST['choices']
  q.correct = req.POST['correct']
  q.save()
  # need the qunit to know where to redirect to
  qunit = q.qunit
  return redirect('/exams/qunits/'+str(qunit.id), RequestContext(req, {}))

@loggedIn
@qAuth
def deleteQ(client, req, id):
  q = Question.objects.get(pk=int(id))
  qunit = q.qunit
  q.delete()
  return redirect('/exams/qunits/'+str(qunit.id), RequestContext(req, {}))

def genUserReport(user):
  # need exam scores for each completed exam
  records = list(Record.objects.filter(kind="exam", user=user))
  records.reverse() # most recent first
  exams = [Exam.objects.get(pk=record.completedId) for record in records]
  reps = []
  for exam in exams:
    right = Response.objects.filter(user=user, exam=exam, correct=True).count()
    total = Response.objects.filter(user=user, exam=exam).count()
    score = 1.0*right/total
    rep = {"title": exam.title, "score": score, "id": exam.id}
    reps.append(rep)
  return reps

@loggedIn
@userAuth
def viewUser(client, req, id):
  user = User.objects.get(pk=int(id))
  exams = user.program.exams.all() # quite a lookup! haha
  ctx = {}
  ctx['user'] = user
  ctx['exams'] = genUserReport(user)
  ctx['client'] = client
  
  responses = list(Response.objects.filter(user=user))
  responses.reverse()
  ctx['responses'] = responses
  
  return render_to_response('exams/viewUser.html', ctx)
  
import csv
def uploadCSV(req):
	try:
		if req.method == 'POST':
			file2 = req.FILES['file']
			print file2
			for line in file2.readlines():
					row = line.replace('"','').replace('\n','').split(',')				
					prog2=Program.objects.get(pk=1)
					prog=Program.objects.get(pk=int(row[4]))
					print prog
					exam = Exam.objects.get(pk=int(row[6]))
					qUnit = QUnit.objects.get(pk=int(row[7]))
					q = Question.objects.get(pk=int(row[8]))
					print exam
					user = User(firstname=row[0], lastname=row[1], bday=row[2], phone=row[3], program=prog, state=row[5], currentExam=exam, 
					currentQUnit=qUnit, currentQ=q)
					user.save()	
					msg = "CSV file successfully imported"	
	except (Exception):
		msg = "Error encountered, please click on 'Choose File' to upload a file"	
	users = User.objects.all()
	return render_to_response('exams/viewAllUsers.html',  locals(), RequestContext(req, {}))

#added April by Vincent
def addUser(req):
    # Create a form instance from POST data.
    # Save a new customer object from the form's data.
    if req.method == 'POST':        
        form = UserForm(req.POST)
        if form.is_valid():
            form.save()            
            msg= "user saved successfully"
            return render_to_response("exams/viewAllUsers.html", {'msg': msg, 'users' :User.objects.all()})
        else:
			msg2 = 'Error in the form!'
			form.error_class
			return render_to_response("exams/addUser.html", locals(),context_instance=RequestContext(req))
    else:
        form = UserForm()        
	return render_to_response("exams/addUser.html", locals(),context_instance=RequestContext(req))
	
def viewAllUsers(req):
	users = User.objects.all()
	return render_to_response('exams/viewAllUsers.html',  locals(),RequestContext(req, {}))

# space!

