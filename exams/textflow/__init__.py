# some of the cool magic that is "textflow"
from core import Machine
from django.utils import simplejson as json

import settings
import helpers

# create a textflow machine using the rules file
def create():
  rulesText = open(settings.RULE_FILE).read()
  rules = json.loads(rulesText) # a list of rules objects
  mach = Machine(rules, settings.handlers)
  print "machine"
  print mach
  return mach


def send_sms(client, user, message):
  helpers.send_sms(client.shortCode, user, message)


def getRule(name):
  rulesText = open(settings.RULE_FILE).read()
  rulesList = json.loads(rulesText)
  # create the rules object
  rules = {}
  for rule in rulesList:
    rules[rule['state']] = rule
  return rules[name]