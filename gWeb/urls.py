from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib.auth.views import password_reset
#from pesaapp.new_form import RegistrationFormZ
#from django.views.generic.simple import direct_to_template
#handle the errors  
from django.views.generic import TemplateView
from django.utils.functional import curry
from django.views.defaults import *


handler404 = curry(page_not_found, template_name='404.html')

#You can also handle other errors like this:
handler500 = curry(server_error, template_name='500.html')
#handler403 = curry(permission_denied, template_name='403.html')
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gWeb.views.home', name='home'),
    # url(r'^gWeb/', include('gWeb.foo.urls')),
    (r'^courses/$',TemplateView.as_view(template_name='index.html')),
	(r'^reports/$',TemplateView.as_view(template_name='reports.html')),
    (r'^help/$',TemplateView.as_view(template_name='help.html')),
    (r'^$',TemplateView.as_view(template_name='home.html')),
    (r'^login$',TemplateView.as_view(template_name='login.html')),
    (r'^questions$','exams.views.questions'),
    (r'^web$','exams.views.receiveWeb')
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
