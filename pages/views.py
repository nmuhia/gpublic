from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse,HttpResponseServerError
from django.shortcuts import render_to_response, redirect
from django.utils import simplejson
from clients.models import Client, Program
from clients.helpers import loggedIn, loggedInUser
from django.db import models
from passlib.hash import sha256_crypt
from exams.models import Exam,User,User, Record, Response,QUnit,Question
#from exams.models import User, Response, QUnit, Question, Exam, Log, Record, UserForm, Content

from django.template import RequestContext

import sys
import AfricasTalkingGateway as ATG
import json


#register new course
@loggedInUser
def newcourse_gweb(guser, req):  
  ctx = {}
  ctx['user'] = guser
  username=req.session['username']
  user=User.objects.get(username=username)
  print user
  return render_to_response('newcourse_gweb.html', locals(),RequestContext(req))


#update user profile with new course details
@loggedInUser
def registerNewCourse(guser,req):
	ctx = {}
	ctx['user'] = guser
	username=req.session['username']
	user=User.objects.get(username=username)
	program_id=req.POST['program_id']
	state="waitResponse"

	try:
		program_results = Program.objects.filter(pk = program_id).count()
		program=Program.objects.get(pk=program_id)

		#get exam id		
		first_exam_id = Exam.objects.filter(program_id=program_id).order_by('id')	
		exam_id=first_exam_id[0].id
		

		#get qunit id
		first_qunit_id=QUnit.objects.filter(exam_id=exam_id).order_by('id')	
		qunit_id=first_qunit_id[0].id
		

		#get question id
		first_question_id=Question.objects.filter(qunit_id=qunit_id).order_by('id')	
		question_id=first_question_id[0].id		

		guser=User.objects.get(username=username)
		guser.program_id=program_id
		guser.currentExam_id=exam_id
		guser.currentQUnit_id=qunit_id
		guser.currentQ_id=question_id
		guser.state=state
		guser.save();

		#guser.update(program=program,currentExam_id=exam_id,currentQUnit_id=qunit_id,currentQ_id=question_id,state=state)

		return redirect('/coursesweb')

		#print program
	
	except Program.DoesNotExist:
		return redirect('/program_error')



#register a user
def registerUser(req):	
	
	firstname=req.POST['firstname']
	lastname=req.POST['lastname']
	username=req.POST['username']
	password=req.POST['password']
	phone=req.POST['phone']
	bday=req.POST['bday']
	program_id=req.POST['program']
	state="waitResponse"
	
	hashed = sha256_crypt.encrypt(password)

	#check if username already exists in db
	username_results = User.objects.filter(username = username).count()
	phone_results = User.objects.filter(phone = phone).count()
	
	#phone_results = User.objects.filter(phone = phone).count()
	try:
		program_results = Program.objects.filter(pk = program_id).count()
		program=Program.objects.get(pk=program_id)

		#get exam id		
		first_exam_id = Exam.objects.filter(program_id=program_id).order_by('id')	
		exam_id=first_exam_id[0].id

		#get qunit id
		first_qunit_id=QUnit.objects.filter(exam_id=exam_id).order_by('id')	
		qunit_id=first_qunit_id[0].id

		#get question id
		first_question_id=Question.objects.filter(qunit_id=qunit_id).order_by('id')	
		question_id=first_question_id[0].id

		print program
	
	except Program.DoesNotExist:
		return redirect('/program_error')
		

	if(username_results>0 or phone_results>0):
		return redirect('/registration_error')	
	else:	
		#register the guser	
		guser = User(firstname=firstname, lastname=lastname, phone=phone,bday=bday,username=username,password=hashed,program=program,currentExam_id=exam_id,currentQUnit_id=qunit_id,currentQ_id=question_id,state=state)		
		guser.save();


		username = 'gmaarifa'
		apikey   = '354e8eb6694f67dbb53133e4527370a8ac408b60b79de8f96c5e3bd6e77bf19d'		

		gateway = ATG.AfricasTalkingGateway(username, apikey)

		
		to="+"+phone
		message="Your g.Maarifa registration is successful. Click on this link to continue\n http://blooming-escarpment-2933.herokuapp.com/m"

		try:
			recipients = gateway.sendMessage(to, message)
			for x in recipients:
		    		print 'number=%s;status=%s;cost=%s' % (x['number'], x['status'], x['cost'])
			return redirect('/confirm_registration')
		
		except:
			e = sys.exc_info()[0]
			print "Error while connecting to the gateway: %s" % e
			#msg = 'Error encountered, message not sent'
			return redirect('/sms_confirm_error')	


#registerAndroid
@csrf_exempt
def registerandroid(req):
	if req.method == 'POST':			
		result=simplejson.loads(req.body)	
    		try:
			firstname=result.get("firstname",None)
			lastname=result.get("surname",None)
			bday=result.get("dob",None)
			phone=result.get("phonenumber",None)
			username = result.get("username",None)
			password=result.get("password",None)
			program_id=result.get("courseid",None)
			state="waitResponse"

			hashed = sha256_crypt.encrypt(password)

			#check if username already exists in db
			username_results = User.objects.filter(username = username).count()
			phone_results = User.objects.filter(phone = phone).count()

			try:
				program_results = Program.objects.filter(pk = program_id).count()
				program=Program.objects.get(pk=program_id)

				#get exam id		
				first_exam_id = Exam.objects.filter(program_id=program_id).order_by('id')	
				exam_id=first_exam_id[0].id

				#get qunit id
				first_qunit_id=QUnit.objects.filter(exam_id=exam_id).order_by('id')	
				qunit_id=first_qunit_id[0].id

				#get question id
				first_question_id=Question.objects.filter(qunit_id=qunit_id).order_by('id')	
				question_id=first_question_id[0].id

				print program
	
			except Program.DoesNotExist:
				#course id does not exists
				return HttpResponse("0")
		

			if(username_results>0 or phone_results>0):
				#user already exists
				return HttpResponse("1")
			else:	
				#register the guser	
				guser = User(firstname=firstname, lastname=lastname, phone=phone,bday=bday,username=username,password=hashed,program=program,currentExam_id=exam_id,currentQUnit_id=qunit_id,currentQ_id=question_id,state=state)		
				guser.save();


				username = 'gmaarifa'
				apikey   = '354e8eb6694f67dbb53133e4527370a8ac408b60b79de8f96c5e3bd6e77bf19d'		

				gateway = ATG.AfricasTalkingGateway(username, apikey)

		
				to="+"+phone
				message="Your g.Maarifa registration is successful."

				try:
					recipients = gateway.sendMessage(to, message)
					for x in recipients:
				    		print 'number=%s;status=%s;cost=%s' % (x['number'], x['status'], x['cost'])
					#registered and SMS sent
					return HttpResponse("2")
		
				except:
					e = sys.exc_info()[0]
					print "Error while connecting to the gateway: %s" % e
					#registered but error sending sms
					return HttpResponse("3")

   		except KeyError:
			e = sys.exc_info()[0]
      			return HttpResponseServerError(e)



#loginAndroid
@csrf_exempt
def loginandroid(req):
	if req.method == 'POST':			
		result=simplejson.loads(req.body)	
    		try:
			username = result.get("username",None)
			password=result.get("password",None)
			guser=User.objects.get(username=username)

			if sha256_crypt.verify(password, guser.password):			
				return HttpResponse(guser.id)
			else:
				return HttpResponse("0")
		except:
			#e = sys.exc_info()[0]
			return HttpResponse("-1")	
	

#loginUser
def logingweb(req):	
	# see if this is a POST to login or just view the form
  	if ('username' in req.POST) and ('password' in req.POST):
	   	username = req.POST.get('username')
	    	password = req.POST.get('password')
		try:
			guser=User.objects.get(username=username)
			#make sure the passwords match
			if sha256_crypt.verify(password, guser.password):
				req.session['username'] = username
				req.session['phone'] = guser.phone
				program = Program.objects.get(id=guser.program.id)
				print guser.program.id
				program = program.programCode
				req.session['program'] = program
				return redirect('/m_home')				
			else:
				#unauthorized
				#error='Invalid username or password'
				return redirect('/login_error')
		except User.DoesNotExist:
			error='Not found'
			# display the form
	    		ctx = {}
	    		ctx.update(csrf(req))
	    		ctx['error'] = error
	    		#return render_to_response('index1.html', ctx)
			return redirect('/login_error')
  	else: # or present login form
    		ctx = {}
    		ctx.update(csrf(req))
    		return render_to_response('index1.html', ctx)
	

def logoutgweb(req):
  del req.session['username']
  del req.session['phone']
  return redirect('/m')

# gweb home page handler
@loggedInUser
def m_home(guser, req):  
  ctx = {}
  ctx['user'] = guser
  username=req.session['username']
  user=User.objects.get(username=username)
  print user
  #return render_to_response('home1.html', ctx)
  return render_to_response('home1.html', locals(),RequestContext(req))

# gweb user reports
def genUserReport(req, user):
  #need exam scores for each completed exam
  #user = User.objects.get(pk=int(id))
  username=req.session['username']
  #user=req.POST.get('username')
  print username
  user=User.objects.get(username=username)
  print user
  records = list(Record.objects.filter(kind="exam", user=user))
  records.reverse() # most recent first
  userprogram=user.program
  print userprogram
  exams = [Exam.objects.get(pk=record.completedId) for record in records]
  reps = []
  for exam in exams:
    right = Response.objects.filter(user=user, exam=exam, correct=True).count()
    total = Response.objects.filter(user=user, exam=exam).count()
    score = 100.0*right/total
    rights=1.0*right
    totals=1.0*total
    rep = {"title": exam.title, "score": score, "rights": rights, "totals": totals, "id": exam.id, "name": user.program}
    reps.append(rep)
  return reps

@loggedIn
def viewUsers(client,req, id):
  user = User.objects.get(pk=int(id))
  records = list(Record.objects.filter(kind="exam", user=user))
  exams = user.program.exams.all() # quite a lookup! haha
  exam=  user.program.exams.all()
  ctx = {}
  ctx['user'] = user
  ctx['exams'] = genUserReport(user)
  #ctx['client'] = client
  
  responses = list(Response.objects.filter(user=user))
  responses.reverse()
  ctx['responses'] = responses
  
  return render_to_response('reportsgweb.html', ctx)  

@loggedInUser
def viewUser(user, req):
  #user = User.objects.get(pk=int(id))
  username=req.session['username']
  #user=req.POST.get('user')
  print username
  user=User.objects.get(username=username)
  print user
  records = list(Record.objects.filter(kind="exam", user=user))
  #client = Client.objects.get(username=username)
  print records
  exams = user.program.exams.all() # quite a lookup! haha
  exam=  user.program.exams.all()
  ctx = {}
  ctx['user'] = user
  ctx['exams'] = genUserReport(req, user)
  #ctx['client'] = client
  
  responses = list(Response.objects.filter(user=user))
  responses.reverse()
  ctx['responses'] = responses
  
  return render_to_response('reportsgweb.html', ctx)
  #return(login)


# handles a POST of login details or a GET
def login(req):
  # see if this is a POST to login or just view the form
  if ('user' in req.POST) and ('pass' in req.POST):
    username = req.POST.get('user')
    password = req.POST.get('pass')
    try:
      client = Client.objects.get(username=username)
      # make sure the passwords match
      if sha256_crypt.verify(password, client.password):
        req.session['clientId'] = client.id
        req.session['phone'] = client.phone
        return redirect('/home')
      else:
        # unauthorized
        error = 'Invalid username or password'
    except Client.DoesNotExist:
      error = 'Not Found'
    
    # display the form
    ctx = {}
    ctx.update(csrf(req))
    ctx['error'] = error
    return render_to_response('pages/login.html', ctx)
  else: # or present login form
    ctx = {}
    ctx.update(csrf(req))
    return render_to_response('pages/login.html', ctx)

def logout(req):
  del req.session['clientId']
  return redirect('/')


# welcome page, no need to be logged in
def welcome(req):
  # do nothing but display the welcome page
  return render_to_response('pages/welcome.html', {})

# home page handler
@loggedIn
def home(client, req):
  programs = client.programs.all()
  ctx = {}
  ctx['programs'] = programs
  ctx['client'] = client
  return render_to_response('pages/home.html', ctx)


#docs page handler
@loggedIn
def docs(client, req):
  return render_to_response('pages/docs.html')


# courses page handler
@loggedIn
def courses(client, req):
	exams = Exam.objects.all()
	programs = client.programs.all()
	ctx = {}
	ctx['programs'] = programs
	ctx['client'] = client
	try:
		myId = req.POST['course']	
	#exams = Exam.objects.all()
		program = Program.objects.get(pk=int(myId))
		ctx = {}
		ctx['program'] = program
	except Exception:
		msg = 'relax'

	return render_to_response('exams/viewExamEdited.html', locals(), RequestContext(req))

@loggedIn
def manage_exams(client, req):
  programs = client.programs.all()
  ctx = {}
  ctx.update(csrf(req))
  ctx['programs'] = programs
  return render_to_response('pages/manage_exams.html', ctx)

@loggedIn
def exams(client, req):
  classes = client.programs.all()
  
  return render_to_response('pages/exams.html')

@loggedIn
def reports(client, req):
  return render_to_response('pages/reports.html')

@loggedIn
def reportsweb(client, req):
  return render_to_response('pages/reportsweb.html')


def console(req):
  ctx = {}
  ctx.update(csrf(req))
  return render_to_response('pages/sms_console.html', ctx)
  
def sms(request):
	return render_to_response('pages/sms.html', locals())
  
