# settings.py sets up TextFlow's special handlers
from clients.models import Program
from datetime import datetime
from exams.models import Exam, QUnit, Question, Record, Response, User

import helpers # get SMS stuff

from django.utils import timezone
from datetime import datetime, timedelta


RULE_FILE = 'exams/rules.json'


# a helper to determine if the user has run out of time
def stillTime(fn):
  def wrapped(user, msg):
    print "about to check the time"
    # get the current exam and figure time
    exam = user.currentExam
    print exam
    # get the current time
    now = datetime.now()
    print now
    # compensate for a 5 min lag for SMS network delays
    lagged = now - timedelta(minutes=5)
    print lagged
    # make it timezone aware so we can compare
    lagged = timezone.make_aware(lagged, timezone.get_default_timezone())
    print "timezone aware"
    print lagged
    #endTime = timezone.make_aware(exam.endTime, timezone.get_default_timezone())
    endTime = exam.endTime
    print "endTime"
    print endTime
    print "lagged:" + str(lagged)
    print "endTime:" + str(endTime)
    #if exam.endTime < lagged:
    if endTime < lagged:
      # time's up!
      print "times up!!!!"
      raise helpers.TimesUpException
    else:
      print "there is time!"
      return fn(user, msg)
  return wrapped


# simple ones first
def firstname(user, msg):
  user.firstname = msg
  user.save()
  return True

def lastname(user, msg):
  user.lastname = msg
  user.save()
  return True

def birthday(user, msg):
  try:
    user.bday = datetime.strptime(msg, "%m.%d.%Y")
    user.save()
    return True
  except ValueError:
    # couldn't parse the bday
    return False

# doesn't use stillTime because the timesUp state needs to route back to this
def nextExam(user, msg):
  # find the next exam that hasn't finished
  now = timezone.make_aware(datetime.now(), timezone.get_default_timezone())
  print now
  print "now"
  exams = user.program.exams.filter(endTime__gt=now, ready=True)
  #exams = user.program.exams.all()
  print "exams"
  print exams
  for exam in exams:
    if not helpers.completed(user, exam):
      print "exam"
      print exam
      user.currentExam = exam
      user.save()
      print "Current exam"
      print user.currentExam
      return True
  # no exams lefts
  return False

@stillTime
def nextQUnit(user, msg):
  # finds the next QUnit, sets user.currentQUnit
  exam = user.currentExam
  for qunit in exam.qunits.order_by('order'):
    if not helpers.completed(user, qunit):
      user.currentQUnit = qunit
      user.save()
      return True
  # no more qunits for this exam, make a Record for the exam
  rec = Record(user=user, kind="exam", completedId=exam.id)
  rec.save()
  return False

@stillTime
def nextQ(user, msg):
  # finds the next question, sets user.currentQ
  qunit = user.currentQUnit
  for q in qunit.questions.order_by('-order'):
    if not helpers.completed(user, q):
      user.currentQ = q
      user.save()
      return True
  # no more questions, make a Record that the qunit is complete
  rec = Record(user=user, kind="qunit", completedId=qunit.id)
  rec.save()
  return False

@stillTime
def checkAnswer(user, msg):
  # checks the answer and makes a Response object
  q = user.currentQ
  qunit = user.currentQUnit
  exam = user.currentExam
  # get rid of any extra spaces and lower case
  lowerStriped = msg.strip().lower()
  right = q.correct.strip().lower()
  correct = lowerStriped == right
  resp = Response(user=user, question=q, qunit=qunit,
                 exam=exam, text=msg, correct=correct)
  resp.save()
  return correct


# only special case handler so far...
# takes three inputs, output is (True, user) or (False, None) for convenience
def makeUser(destination, source, message):
  # make the user object
  query = Program.objects.filter(programCode=message)
  if not query:
    # didn't send a valid code...
    return (False, None)

  # otherwise all good, create the user
  program = query[0]
  user = User(phone=source, program=program)
  user.save()
  return (True, user)

# add all the handlers

handlers = {}
handlers['firstName'] = firstname
handlers['lastName'] = lastname
handlers['birthday'] = birthday
handlers['nextExam'] = nextExam
handlers['nextQUnit'] = nextQUnit
handlers['nextQ'] = nextQ
handlers['checkAnswer'] = checkAnswer

# special handlers
handlers['makeUser'] = makeUser

