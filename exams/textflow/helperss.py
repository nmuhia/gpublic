# helper functions for texting
from django.template import Context, Template
from exams.models import Log, Exam, QUnit, Question, Response, Record

class TimesUpException(Exception):
  pass

def send_sms(client, user, message):
  # set up the "magic" variables for the rules.json
  ctx = {}
  ctx['first'] = user.firstname
  ctx['last'] = user.lastname
  if user.currentExam:
    ctx['exam'] = user.currentExam.title
    ctx['endTime'] = user.currentExam.endTime
  if user.currentQUnit:
    ctx['qunit'] = user.currentQUnit.title
    ctx['qunitDescription'] = user.currentQUnit.description
  if user.currentQ:
    ctx['question'] = user.currentQ.text
    ctx['choices'] = user.currentQ.choices
    ctx['answer'] = user.currentQ.correct
  t = Template(message)
  text = t.render(Context(ctx))
  
  # make a log, not sending just yet
  log = Log(source=client.shortCode, destination=user.phone, message=text, mock=client.mock)
  log.save()
  
  # determine if message really has to be sent out
  if client.mock == False:
    #gateway_url = "http://sms.shujaa.com/sendsms"
    gateway_url = "http://sms.shujaa.mobi/sendsms"
    data = {}
    data['username'] = client.smsUser
    data['password'] = client.smsPass
    data['account'] = 'live'
    data['source'] = client.shortCode
    data['destination'] = '254'+user.phone[1:]
    data['message'] = text
    encodedData = urllib.encode(data)
    urllib.urlopen(gateway_url, encodedData)


def send_sms_direct(shortCode, phone, message):
  log = Log(source=shortCode, destination=phone, message=message)
  log.save()

def send_question(user, question):
  send_sms(user, question.text)
  send_sms(user, question.choices)


def completed(user, obj):
  res = False # assume not
  # first determine if it's a QUnit or an Exam
  if type(obj) == Exam:
    res = Record.objects.filter(kind='exam', completedId=obj.id, user=user)
  if type(obj) == QUnit:
    res = Record.objects.filter(kind='qunit', completedId=obj.id, user=user)
  if type(obj) == Question:
    res = Response.objects.filter(user=user, question=obj)
  return res
